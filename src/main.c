/*
 ▄▄       ▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░░▌     ▐░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌░▌   ▐░▐░▌ ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌▐░▌ ▐░▌▐░▌     ▐░▌     ▐░▌       ▐░▌▐░▌          
▐░▌ ▐░▐░▌ ▐░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ 
▐░▌  ▐░▌  ▐░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌   ▀   ▐░▌     ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀█░▌
▐░▌       ▐░▌     ▐░▌     ▐░▌                    ▐░▌
▐░▌       ▐░▌ ▄▄▄▄█░█▄▄▄▄ ▐░▌           ▄▄▄▄▄▄▄▄▄█░▌
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌
 ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀            ▀▀▀▀▀▀▀▀▀▀▀ 

   _____ _                 _       _   _                                                         
  / ____(_)               | |     | | (_)                                                        
 | (___  _ _ __ ___  _   _| | __ _| |_ _  ___  _ __                                              
  \___ \| | '_ ` _ \| | | | |/ _` | __| |/ _ \| '_ \                                             
  ____) | | | | | | | |_| | | (_| | |_| | (_) | | | |                                            
 |_____/|_|_| |_| |_|\__,_|_|\__,_|\__|_|\___/|_| |_|                                            
  _____                _______ _                      _     _ _       __     __                   
 |  __ \           _  |__   __| |                    | |   (_) |      \ \   / /                   
 | |__) |_ _ _ __ (_)    | |  | |__   ___  ___  _ __ | |__  _| | ___   \ \_/ /_   ____ _ _ __ ___ 
 |  ___/ _` | '__|       | |  | '_ \ / _ \/ _ \| '_ \| '_ \| | |/ _ \   \   /\ \ / / _` | '__/ __|
 | |  | (_| | |    _     | |  | | | |  __/ (_) | |_) | | | | | |  __/    | |  \ V / (_| | |  \__ \
 |_|   \__,_|_|   (_)    |_|  |_| |_|\___|\___/| .__/|_| |_|_|_|\___|    |_|   \_/ \__,_|_|  |___/
                                               | |                                                
                                               |_|                                                
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "../headers/CPU.h"
#include "../headers/ordonnanceur.h"
#include "../headers/instruction.h"
#include "../headers/registerFile.h"
#include "../headers/boolean.h"
#include "../headers/writeProg.h"
#include "../headers/memoire.h"
#include "../headers/controlUnit.h"
#include "../headers/color.h"

pthread_mutex_t my_mutex = PTHREAD_MUTEX_INITIALIZER; // J'utilise un mutex pour que l'affichage des threads ne se mélange pas
pthread_mutex_t mutexOrdonnanceur = PTHREAD_MUTEX_INITIALIZER; // J'utilise un mutex pour que l'affichage des threads ne se mélange pas

int horloge;
int finProgramme; 

enum step{
    IF,
    ID,
    EX,
    MEM,
    WB,
    BULLE
};

/*
    Thread qui simule un petit ordonnaceur.
*/

void *Ordonnanceur(void *arg){ // Thread d'excecution
// Mettre en place un ordonnaceur
    short tampon;
    int start;

    while(unitControlRegister.ordonnanceur == 0){
        tampon = horloge%2;
        usleep(20000);
        start = startOrdonnanceur();

        for(int x = 0; x < 10; x++){
            ordonnanceur[(start+x)%10] = -1;
            usleep(15000);
        }

        while(tampon == horloge%2){
            // Attend l'horloge
        }
    }
    pthread_exit(NULL);
}

void *etage(void *arg){ // Thread d'excecution
   
    int nbInstruction = *((int *)arg); // on fait un cast vers le pointeur voulu
    
    int finThread = false;
    int process = 0;
    int tampon;

    // Gestion du réseaux circuit electrique/electronique
    short RTzero;
    char PCsequentiel;
    short valeurALU;
    short optocode;
    char RT;
    short IR;
    short RD;
    char RS;
    short IMM;

    short tab[10]; // stock le cycle des instructions, pour l'affichage de ceci
    short niveau = -1;

    short RTadresse = 0;
    short RSadresse = 0;

    short verrou = -1; // verrou julmp
    short verrouBranch = -1; // verrou branch

    short maskRD = 3840; // 0b0000111100000000
    short maskS1 = 240; // 0b0000000011110000
    short maskS2 = 15; // 0b0000000000001111

    short pc; // simulation du cable

    for(int y = 0; y < 10; y++)
        tab[y] = -1;

    while(!finThread){  
        niveau ++; 
        pthread_mutex_lock(&mutexOrdonnanceur);
        saveOrdonnanceur(nbInstruction);
        pthread_mutex_unlock(&mutexOrdonnanceur);

        while(ordonnanceur[nbInstruction%10] != -1){
            // Attente de l'ordonnanceur
        }
        pthread_mutex_lock(&my_mutex); // Permet d'avoir un affichage lisible. et non pas des printf qui se mélange d'un thread à l'autre

        printf("\n[ "YELLOW"Horloge"DEFAULT_COLOR" ] <- %d\n",horloge); 

        switch (process)
        {
            case IF:
/* ========================================================================

                                ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
                                ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
                                ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ 
                                    ▐░▌     ▐░▌          
                                    ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ 
                                    ▐░▌     ▐░░░░░░░░░░░▌
                                    ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀ 
                                    ▐░▌     ▐░▌          
                                ▄▄▄▄█░█▄▄▄▄ ▐░▌          
                                ▐░░░░░░░░░░░▌▐░▌          
                                ▀▀▀▀▀▀▀▀▀▀▀  ▀           
                          
*///=======================================================================
                
                // Si branchement detecté, ce thread se stop ou si fin prog
                
                if((unitControlRegister.Branchement == 1) || (unitControlRegister.STOP == 1)){ 
                    finThread = true;
                    pthread_mutex_unlock(&my_mutex);
                    break;
                }

                // IF
                if(unitControlRegister.aleasDonnees == 0){
                    printf(CYAN"Instruction %d : "DEFAULT_COLOR, nbInstruction);
                    tab[niveau] = 0;
                    unitcontrolePrintCycle(tab, niveau);
                    printf("\n");
                    
                    pc = unitControlRegister.PC;
                    unitControlRegister.IR = instructionMemory(pc); // Recupere le mot memoire[pc] et memoire[pc+1]

                    printf("[ "GREEN"PC"DEFAULT_COLOR" ] <- %d\n[ "GREEN"IR"DEFAULT_COLOR" ] <- %d\n", unitControlRegister.PC, unitControlRegister.IR);
                    pc = unitControlPcHorloge(adder2(pc));
                    
                    // Gestion du circuit electrique

                    IR =  unitControlRegister.IR;
                    PCsequentiel = pc;

                    // Gestion du cycle d'horloge

                    pthread_mutex_unlock(&my_mutex);
                    tampon = horloge%2;
                    while(tampon == horloge%2){
                        // Attend l'horloge
                    }

                    if((unitControlRegister.Branchement == 1) || (unitControlRegister.STOP == 1)){ 
                        finThread = true;
                        pthread_mutex_unlock(&my_mutex);
                        break;
                    }

                    process = ID;
                    break;
                }
                else{
                    printf(CYAN"Instruction %d : "DEFAULT_COLOR, nbInstruction);
                    tab[niveau] = 5;
                    unitcontrolePrintCycle(tab, niveau);
                    printf("\n");
                    pthread_mutex_unlock(&my_mutex);
                    tampon = horloge%2;
                    while(tampon == horloge%2){
                        // Attend l'horloge
                    }
                    break;
                }

            case ID:
/* ========================================================================

                                ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄  
                                ▐░░░░░░░░░░░▌▐░░░░░░░░░░▌ 
                                ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌
                                    ▐░▌     ▐░▌       ▐░▌
                                    ▐░▌     ▐░▌       ▐░▌
                                    ▐░▌     ▐░▌       ▐░▌
                                    ▐░▌     ▐░▌       ▐░▌
                                    ▐░▌     ▐░▌       ▐░▌
                                ▄▄▄▄█░█▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌
                                ▐░░░░░░░░░░░▌▐░░░░░░░░░░▌ 
                                ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀  
                                   
                          
*///=======================================================================
               
                // Si branchement detecté, ce thread se stop ou si fin prog
                
                if((unitControlRegister.Branchement == 1) || (unitControlRegister.STOP == 1)){ 
                    finThread = true;
                    pthread_mutex_unlock(&my_mutex);
                    break;
                }
                
                // ID
                if(unitControlRegister.aleasDonnees == 0){
                    printf(CYAN"Instruction %d : "DEFAULT_COLOR, nbInstruction);
                    tab[niveau] = 1;
                    unitcontrolePrintCycle(tab, niveau);
                    printf("\n");
                    
                    unitControlRegister.optocode = unitControlOptocode(IR);
                    printf("[ "GREEN"Optocode"DEFAULT_COLOR" ] <- %d : ",unitControlRegister.optocode);
                    unitControlPrintOptocode(unitControlRegister.optocode);

                    unitControlRegister.RD = (IR & maskRD) >> 8; // RD
                    unitControlRegister.RS = (IR & maskS1) >> 4; // S1
                    unitControlRegister.RT = (IR & maskS2);      // S2
                    unitControlRegister.RSadresse = (IR & maskS1) >> 4;
                    unitControlRegister.RTadresse = (IR & maskS2);
                    RTadresse = unitControlRegister.RSadresse;
                    RSadresse = unitControlRegister.RTadresse;

                    
                    unitControlRegister.IMM = unitControlIMM(unitControlRegister.optocode, unitControlRegister.IR);


                    // Gestion du circuit electrique 
                    IMM = unitControlRegister.IMM;
                    RD = unitControlRegister.RD;
                    optocode = unitControlRegister.optocode;
                    
                    if( (optocode == branch_or_equal) || (optocode == branch_on_not_equal) ){
                        RS = unitControlRegister.RS;
                        RT = unitControlRegister.RT;
                        printf("Registre de condition : ( "GREEN"$%d"DEFAULT_COLOR" ) ( "GREEN"$%d"DEFAULT_COLOR" ) -> saut : ( "GREEN"%d"DEFAULT_COLOR" )\n",RD,RS,RT);
                    }else if(optocode == jump){
                        printf("Adresse jump : ( "GREEN"%d"DEFAULT_COLOR" ) \n",RD);
                    }else{
                        
                        RS = registreFile[(short)unitControlRegister.RS];
                        RT = registreFile[(short)unitControlRegister.RT];
                        printf("[ "GREEN"RD"DEFAULT_COLOR" ] <- $%d\n",(IR & maskRD) >> 8);
                        printf("[ "GREEN"RS($%d)"DEFAULT_COLOR" ] <- %d\n",(IR & maskS1) >> 4,registreFile[(IR & maskS1) >> 4]);
                        printf("[ "GREEN"RT($%d)"DEFAULT_COLOR" ] <- %d\n",(IR & maskS2),registreFile[(IR & maskS2)]);
                        printf("[ "GREEN"IMM"DEFAULT_COLOR" ] <- %d\n",IMM);
                    }

                

                    // Gestion du cycle d'horloge

                    pthread_mutex_unlock(&my_mutex);
                    tampon = horloge%2;
                    while(tampon == horloge%2){
                        // Attend l'horloge
                    }

                    if((unitControlRegister.Branchement == 1) || (unitControlRegister.STOP == 1)){ 
                        finThread = true;
                        pthread_mutex_unlock(&my_mutex);
                        break;
                    }

                    process = EX;
                    break;
                }
                else{
                    printf(CYAN"Instruction %d : "DEFAULT_COLOR, nbInstruction);
                    tab[niveau] = 5;
                    unitcontrolePrintCycle(tab, niveau);
                    printf("\n");

                    pthread_mutex_unlock(&my_mutex);
                    tampon = horloge%2;
                    while(tampon == horloge%2){
                        // Attend l'horloge
                    }
                    process = ID;
                    break;
                }
            
            case EX:
/* ========================================================================

                                 ▄▄▄▄▄▄▄▄▄▄▄  ▄       ▄ 
                                ▐░░░░░░░░░░░▌▐░▌     ▐░▌
                                ▐░█▀▀▀▀▀▀▀▀▀  ▐░▌   ▐░▌ 
                                ▐░▌            ▐░▌ ▐░▌  
                                ▐░█▄▄▄▄▄▄▄▄▄    ▐░▐░▌   
                                ▐░░░░░░░░░░░▌    ▐░▌    
                                ▐░█▀▀▀▀▀▀▀▀▀    ▐░▌░▌   
                                ▐░▌            ▐░▌ ▐░▌  
                                ▐░█▄▄▄▄▄▄▄▄▄  ▐░▌   ▐░▌ 
                                ▐░░░░░░░░░░░▌▐░▌     ▐░▌
                                 ▀▀▀▀▀▀▀▀▀▀▀  ▀       ▀ 
                        
                                   
                          
*///=======================================================================
                
                // Si branchement detecté, ce thread se stop ou si fin prog
                
                if((unitControlRegister.Branchement == 1) || (unitControlRegister.STOP == 1)){
                    finThread = true;
                    pthread_mutex_unlock(&my_mutex);
                    break;
                }
                tab[niveau] = 2;
                 // Controle Aleas de données
                unitControlRegister.lecture1 = RS;
                unitControlRegister.lecture2 = RT;
                if( (optocode == branch_or_equal) || (optocode == branch_on_not_equal) ){
                    unitControlRegister.lecture1 = RD;
                    unitControlRegister.lecture2 = RS;
                    printf("Registre de lecture1 : ("GREEN" $%d "DEFAULT_COLOR")\n",unitControlRegister.lecture1);
                    printf("Registre de lecture2 : ("GREEN" $%d "DEFAULT_COLOR")\n",unitControlRegister.lecture2);

                    if( (unitControlRegister.lecture1 == unitControlRegister.ecriture) || (unitControlRegister.lecture2 == unitControlRegister.ecriture) ){
                        printf(RED"Aleas de donnees ! \n"DEFAULT_COLOR);
                        unitControlRegister.aleasDonnees = 1;
                        tab[niveau] = 5;
                    }else{
                        unitControlRegister.aleasDonnees = 0;
                    }
                }


                printf(CYAN"Instruction %d : "DEFAULT_COLOR, nbInstruction);
                unitcontrolePrintCycle(tab, niveau);
                printf("\n");
                
                // EX

              
                if(unitControlRegister.aleasDonnees == 0){
                    if(optocode == branch_or_equal){
                        if(registreFile[RD] == registreFile[(short)RS])
                            RTzero = 1;
                        
                    }else if(optocode == branch_on_not_equal){
                        if(registreFile[RD] != registreFile[(short)RS])
                            RTzero = 1;
                    
                    }else{ 
                        unitControlRegister.lecture1 = RSadresse;
                        unitControlRegister.lecture2 = RTadresse;
                        printf("Registre de lecture1 : ("GREEN" $%d "DEFAULT_COLOR")\n",unitControlRegister.lecture1);
                        printf("Registre de lecture2 : ("GREEN" $%d "DEFAULT_COLOR")\n",unitControlRegister.lecture2);
                    }

                    if(optocode == jump){
                        RTzero = zero(RT); // observe si RT = 0; branch
                        unitControlRegister.Branchement = 1;
                        verrou = nbInstruction;
                    }
                    printf("[ "GREEN"zero"DEFAULT_COLOR" ] <- %d\n",RTzero);
                    unitControlCommandMUX(optocode);// fonction pour les commandes des MUXs
                    printf("[ "GREEN"MUXsup"DEFAULT_COLOR" ] <- %d\n",unitControlRegister.ALUSrcA);
                    printf("[ "GREEN"MUXinf"DEFAULT_COLOR" ] <- %d\n",unitControlRegister.ALUSrcB);
                    

                    // Gestion du circuit electrique

                    valeurALU = ALU(muxSup(PCsequentiel,RS,unitControlRegister.ALUSrcA),muxInf(RD,RT,2,IMM,unitControlRegister.ALUSrcB),optocode);
                    printf("[ "GREEN"ALU"DEFAULT_COLOR" ] <- %d\n",valeurALU);
                
                    if( (optocode == branch_or_equal) && (valeurALU) ){
                        printf("branch_or_equal : "GREEN"VALIDE\n"DEFAULT_COLOR);
                        RTzero = 1;
                        unitControlRegister.Branchement = 1;
                        verrouBranch = nbInstruction;
                    }

                    // Gestion du cycle d'horloge

                    pthread_mutex_unlock(&my_mutex);
                    tampon = horloge%2;
                    while(tampon == horloge%2){
                        // Attend l'horloge
                    }
                    

                    process = MEM;
                    break;

    //                if((unitControlRegister.Branchement == 1) /*|| (unitControlRegister.STOP == 1)*/){ 
    //                   finThread = true;
    //                    pthread_mutex_unlock(&my_mutex);
    //                    break;
    //                }
                }else{
                    pthread_mutex_unlock(&my_mutex);
                    tampon = horloge%2;
                    while(tampon == horloge%2){
                        // Attend l'horloge
                    }
                    break;
                }

            case MEM:
/* ========================================================================

                                 ▄▄       ▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄       ▄▄ 
                                ▐░░▌     ▐░░▌▐░░░░░░░░░░░▌▐░░▌     ▐░░▌
                                ▐░▌░▌   ▐░▐░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌░▌   ▐░▐░▌
                                ▐░▌▐░▌ ▐░▌▐░▌▐░▌          ▐░▌▐░▌ ▐░▌▐░▌
                                ▐░▌ ▐░▐░▌ ▐░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌ ▐░▐░▌ ▐░▌
                                ▐░▌  ▐░▌  ▐░▌▐░░░░░░░░░░░▌▐░▌  ▐░▌  ▐░▌
                                ▐░▌   ▀   ▐░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌   ▀   ▐░▌
                                ▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌
                                ▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌
                                ▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌
                                ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀ 
                                       
                        
                                   
                          
*///=======================================================================
                
                // Si branchement detecté, ce thread se stop ou si fin prog
               
               if(/*(unitControlRegister.Branchement == 1) ||*/ (unitControlRegister.STOP == 1)){ 
                    finThread = true;
                    pthread_mutex_unlock(&my_mutex);
                    break;
                }

                // MEM    
                
                printf(CYAN"Instruction %d : "DEFAULT_COLOR, nbInstruction);
                tab[niveau] = 3;
                unitcontrolePrintCycle(tab, niveau);
                printf("\n");


                if(RTzero == 1){ // branchement
                    //unitControlRegister.Branchement = 1;


                    if( (optocode == branch_or_equal) && valeurALU == 1 ){
                        printf("[ "GREEN"branch"DEFAULT_COLOR" ] <- %d\n",unitControlRegister.PC);
                        unitControlRegister.PC = (PCsequentiel+1) + 2 + RT;
                        printf("Nouvelle adresse : ( "GREEN"$%d"DEFAULT_COLOR" )\n",unitControlRegister.PC);
                    }
                    if(optocode == jump){
                        printf("[ "GREEN"branch"DEFAULT_COLOR" ] <- %d\n",unitControlRegister.PC);
                        unitControlRegister.PC = valeurALU;
                        //finThread = true;
                    }

                    
                    
                }else if(optocode == load_word){ // load
                    unitControlRegister.ecriture = valeurALU;
                    printf("Registre de ecriture : ("GREEN" $%d "DEFAULT_COLOR")\n",unitControlRegister.ecriture);
                    memoire[valeurALU] = RS;
                    printf("memoire[ "GREEN"%d"DEFAULT_COLOR" ] <- %d\n",valeurALU,RS);
                }else{ // sw
                    printf("sw : chargement pour WB\n");
                    
/*                    unitControlRegister.memoryAdress = valeurALU;
                    memoryAdress = unitControlRegister.memoryAdress;
                    printf("[ "GREEN"memoryAdress"DEFAULT_COLOR" ] <- %d\n",memoryAdress);

                    if(optocode == load_word){
                        unitControlRegister.dataRegister = memoire[unitControlRegister.memoryAdress];  
                    }
                    if(optocode == store_word){
                        unitControlRegister.dataRegister = registreFile[unitControlRegister.memoryAdress];
                    }

                     // Gestion du circuit electrique
                        
                    dataRegister = unitControlRegister.dataRegister;
                    printf("[ "GREEN"dataRegister"DEFAULT_COLOR" ] <- %d\n",dataRegister);
*/
                }

                


                // Gestion de l'horloge
                
                tampon = horloge%2;
                pthread_mutex_unlock(&my_mutex);     
                while(tampon == horloge%2){
                    // Attend l'horloge
                }
                unitControlRegister.ecriture = -1;
                /*if((unitControlRegister.Branchement == 1) || (unitControlRegister.STOP == 1)){ 
                    finThread = true;
                    pthread_mutex_unlock(&my_mutex);
                    break;
                }*/

   

                process = WB;
                break;

            case WB:
/* ========================================================================

                                 ▄         ▄  ▄▄▄▄▄▄▄▄▄▄  
                                ▐░▌       ▐░▌▐░░░░░░░░░░▌ 
                                ▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀█░▌
                                ▐░▌       ▐░▌▐░▌       ▐░▌
                                ▐░▌   ▄   ▐░▌▐░█▄▄▄▄▄▄▄█░▌
                                ▐░▌  ▐░▌  ▐░▌▐░░░░░░░░░░▌ 
                                ▐░▌ ▐░▌░▌ ▐░▌▐░█▀▀▀▀▀▀▀█░▌
                                ▐░▌▐░▌ ▐░▌▐░▌▐░▌       ▐░▌
                                ▐░▌░▌   ▐░▐░▌▐░█▄▄▄▄▄▄▄█░▌
                                ▐░░▌     ▐░░▌▐░░░░░░░░░░▌ 
                                ▀▀       ▀▀  ▀▀▀▀▀▀▀▀▀▀  
                                       
                        
                                   
                          
*///=======================================================================
                printf(CYAN"Instruction %d : "DEFAULT_COLOR, nbInstruction);
                tab[niveau] = 4;
                unitcontrolePrintCycle(tab, niveau);
                printf("\n");

                if(optocode == store_word){
                    unitControlRegister.ecriture = RD;
                    printf("Registre de ecriture : ("GREEN" $%d "DEFAULT_COLOR")\n",unitControlRegister.ecriture);
                    registreFile[RD] = valeurALU;
                    printf("registerFile ["GREEN" %d "DEFAULT_COLOR"] <- %d\n",RD,valeurALU);
                }else if(optocode == load_word){
                    printf("lw : rien à faire ici\n");
                }else if( (optocode >= add) && (optocode <= multiply) ){
                    registreFile[RD] = valeurALU;
                    unitControlRegister.ecriture = RD;
                    printf("Registre de ecriture : ("GREEN" $%d "DEFAULT_COLOR")\n",unitControlRegister.ecriture);
                    printf("registerFile ["GREEN" $%d "DEFAULT_COLOR"] <- %d\n",RD,valeurALU);
                }
                
                pthread_mutex_unlock(&my_mutex);
                tampon = horloge%2;
                while(tampon == horloge%2){
                    // Attend l'horloge
                }
                unitControlRegister.ecriture = -1;

                finThread = true;
                if( (optocode == jump) && (verrou == nbInstruction)){
                        unitControlRegister.Branchement = 0;
                }
                if( (optocode == branch_or_equal) && (verrouBranch == nbInstruction)){
                        unitControlRegister.Branchement = 0;
                }
                
                break;
                
            
            default:
                break;
        }
    } 
    
    pthread_exit(NULL);
}

























/*
 ▄            ▄▄▄▄▄▄▄▄▄▄▄                      ▄▄       ▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄        ▄ 
▐░▌          ▐░░░░░░░░░░░▌                    ▐░░▌     ▐░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░▌      ▐░▌
▐░▌          ▐░█▀▀▀▀▀▀▀▀▀                     ▐░▌░▌   ▐░▐░▌▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀█░█▀▀▀▀ ▐░▌░▌     ▐░▌
▐░▌          ▐░▌                              ▐░▌▐░▌ ▐░▌▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌▐░▌    ▐░▌
▐░▌          ▐░█▄▄▄▄▄▄▄▄▄                     ▐░▌ ▐░▐░▌ ▐░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌     ▐░▌ ▐░▌   ▐░▌
▐░▌          ▐░░░░░░░░░░░▌                    ▐░▌  ▐░▌  ▐░▌▐░░░░░░░░░░░▌     ▐░▌     ▐░▌  ▐░▌  ▐░▌
▐░▌          ▐░█▀▀▀▀▀▀▀▀▀                     ▐░▌   ▀   ▐░▌▐░█▀▀▀▀▀▀▀█░▌     ▐░▌     ▐░▌   ▐░▌ ▐░▌
▐░▌          ▐░▌                              ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌    ▐░▌▐░▌
▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄                     ▐░▌       ▐░▌▐░▌       ▐░▌ ▄▄▄▄█░█▄▄▄▄ ▐░▌     ▐░▐░▌
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌                    ▐░▌       ▐░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌      ▐░░▌
 ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀                      ▀         ▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀        ▀▀ 
                                                                                                  
*/



int main(int argc, char *argv[]){
    if(argc < 3){
        printf(RED"\n\nERROR : [ Fichiers instruction et/ou donnée manquant(s) ]\n\n"DEFAULT_COLOR);
        printf("Rappel : ./MIPS 'instruction'.txt 'data'.txt\n\n");
        exit(EXIT_FAILURE);
    }
    
    // ======================================================================================================
    //                                                                                                         INITIALISATION

    initFileRegistre();
    unitControlInit();
    initMemoire();
    initOrdionnanceur();
    finProgramme = false;

    // ======================================================================================================
    //                                                                                                         TITRE

    printf(PURPLE"\n\n\
 ▄▄       ▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ \n\
▐░░▌     ▐░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌\n\
▐░▌░▌   ▐░▐░▌ ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ \n\
▐░▌▐░▌ ▐░▌▐░▌     ▐░▌     ▐░▌       ▐░▌▐░▌          \n\
▐░▌ ▐░▐░▌ ▐░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ \n\
▐░▌  ▐░▌  ▐░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌\n\
▐░▌   ▀   ▐░▌     ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀█░▌\n\
▐░▌       ▐░▌     ▐░▌     ▐░▌                    ▐░▌\n\
▐░▌       ▐░▌ ▄▄▄▄█░█▄▄▄▄ ▐░▌           ▄▄▄▄▄▄▄▄▄█░▌\n\
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌\n\
 ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀            ▀▀▀▀▀▀▀▀▀▀▀ \n\
\n\n"DEFAULT_COLOR);

    // ======================================================================================================
    //                                                                                                         ENREGISTREMENT DES FICHIERS DATAS ET INSTRUCTIONS

    printf("\n********************************************************************\n\n"); //big
    sleep(1);
    printf(CYAN"\
  _____           _                   _   _             \n\
 |_   _|         | |                 | | (_)            \n\
   | |  _ __  ___| |_ _ __ _   _  ___| |_ _  ___  _ __  \n\
   | | | '_ \x5c/ __| __| '__| | | |/ __| __| |/ _ \x5c| '_ \x5c \n\
  _| |_| | | \x5c__ \x5c |_| |  | |_| | (__| |_| | (_) | | | |\n\
 |_____|_| |_|___/\x5c__|_|   \x5c__,_|\x5c___|\x5c__|_|\x5c___/|_| |_|\n\
                                                        \n\
                                                        \n\
    "DEFAULT_COLOR);
    printf("\n________________________________________________________________");
    printf("\n                |                       |");
    printf("\n     "GREEN"ADRESSE"DEFAULT_COLOR"    |    "GREEN"MOT EN BINAIRE"DEFAULT_COLOR"     |      "GREEN"MOT EN DECIMAL"DEFAULT_COLOR);
    printf("\n________________|_______________________|_______________________");
    printf("\n                |                       |                       \n");

    writeData(argv[1],instruction);

    printf("________________|_______________________|_______________________\n");
    sleep(1);
    printf(CYAN"\n\n\
  _____        _        \n\
 |  __ \x5c      | |       \n\
 | |  | | __ _| |_ __ _ \n\
 | |  | |/ _` | __/ _` |\n\
 | |__| | (_| | || (_| |\n\
 |_____/ \x5c__,_|\x5c__\x5c__,_|\n\
\n\n"DEFAULT_COLOR);

    printf("\n________________________________________________________________");
    printf("\n                |                       |");
    printf("\n     "GREEN"ADRESSE"DEFAULT_COLOR"    |    "GREEN"MOT EN BINAIRE"DEFAULT_COLOR"     |      "GREEN"MOT EN DECIMAL"DEFAULT_COLOR);
    printf("\n________________|_______________________|_______________________");
    printf("\n                |                       |                       \n");
    
    writeData(argv[2],data);

    printf("________________|_______________________|_______________________");
    printf("\n\n\n****************************************************************\n\n");
    sleep(1);
    printf(CYAN"\
  ________   ________ _____ _    _ _______ _____ ____  _   _ \n\
 |  ____\x5c \x5c / /  ____/ ____| |  | |__   __|_   _/ __ \x5c| \x5c | |\n\
 | |__   \x5c V /| |__ | |    | |  | |  | |    | || |  | |  \x5c| |\n\
 |  __|   > < |  __|| |    | |  | |  | |    | || |  | | . ` |\n\
 | |____ / . \x5c| |___| |____| |__| |  | |   _| || |__| | |\x5c  |\n\
 |______/_/ \x5c_\x5c______\x5c_____|\x5c____/   |_|  |_____\x5c____/|_| \x5c_|\n\
    "DEFAULT_COLOR);

    sleep(1);

    // ======================================================================================================
    //                                                                                                         BOUCLE D EXECUTION DES THREADS
    pthread_t threadInstruction; // Création d'une variable thread
    pthread_t threadOrdonnanceur; // Création d'une variable thread
    sleep(1);

    /*
        Lancement de l'ordonnanceur;
    */
    if(pthread_create(&threadOrdonnanceur, NULL, Ordonnanceur, (void *)&horloge) == -1) {
        perror("pthread1_create");
        return EXIT_FAILURE;
    }

    int instruct = 0;

    /*
        Lancement du programme
    */
    while(!finProgramme){
        //-----------------------------------------------------------------------------------// Execute la fonction thread
        printf("\n\n****************************************************************\n\n");
        
        if(unitControlRegister.aleasDonnees == 0){
            if(pthread_create(&threadInstruction, NULL, etage, (void *)&instruct) == -1) {
                perror("pthread1_create");
                return EXIT_FAILURE;
            }
            instruct ++;
        }

        sleep(1); //Horloge
        
              //      ----------------------------------------------------------------

        if(unitControlRegister.STOP == 1){
            finProgramme = 1;
        }
        printf("\n\n****************************************************************\n\n");
        horloge ++;
    }

    /*
        Permet de finir l'execution des instructions en fin de traitement
    */
    printf("\n\n\n****************************************************************\n\n");
    sleep(1);
    horloge ++;
    sleep(1);
    horloge ++;
    sleep(1);
    horloge ++;
    sleep(1);
    unitControlRegister.ordonnanceur = 1;
    printf("\n\n\n****************************************************************\n\n");

    printf(PURPLE"\
 ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄        ▄ \n\
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░▌      ▐░▌\n\
▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀█░█▀▀▀▀ ▐░▌░▌     ▐░▌\n\
▐░▌               ▐░▌     ▐░▌▐░▌    ▐░▌\n\
▐░█▄▄▄▄▄▄▄▄▄      ▐░▌     ▐░▌ ▐░▌   ▐░▌\n\
▐░░░░░░░░░░░▌     ▐░▌     ▐░▌  ▐░▌  ▐░▌\n\
▐░█▀▀▀▀▀▀▀▀▀      ▐░▌     ▐░▌   ▐░▌ ▐░▌\n\
▐░▌               ▐░▌     ▐░▌    ▐░▌▐░▌\n\
▐░▌           ▄▄▄▄█░█▄▄▄▄ ▐░▌     ▐░▐░▌\n\
▐░▌          ▐░░░░░░░░░░░▌▐░▌      ▐░░▌\n\
 ▀            ▀▀▀▀▀▀▀▀▀▀▀  ▀        ▀▀ \n\
    "DEFAULT_COLOR);

   

    printf("\n\n");
    return EXIT_SUCCESS;
}
