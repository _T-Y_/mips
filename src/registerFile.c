#include <stdio.h>

#include "../headers/registerFile.h"
#include "../headers/CPU.h"

void initFileRegistre(){
    for(int i = 0; i < 16; i++)
        registreFile[i] = 0;

    registreFile[5] = 3;
    registreFile[6] = 2;
    registreFile[8] = 5;
    registreFile[9] = 1;
    registreFile[11] = 10;
    registreFile[12] = 7;
}