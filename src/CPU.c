#include <stdio.h>
#include <stdlib.h>

#include "../headers/CPU.h"
#include "../headers/instruction.h"
#include "../headers/memoire.h"
#include "../headers/registerFile.h"
#include "../headers/controlUnit.h"
#include "../headers/color.h"

/*
Simulation additionneur complet , +2
*/
short adder2(short pc){
    short increment = 2; // PC + 2
    
    short A = 0;
    short B = 0;
    short Ri_1 = 0;
    short Ri = 0;
    short S = 0;

    // Variables G(x) pour les états des portes logiques dans l'additionneur
    short G1 = 0;
    short G2 = 0;
    short G3 = 0;

    short mask = 1;

    short resultat = 0;

    for(int i = 0; i < 8; i++){
        A = (pc & mask) >> i;
        B = (increment & mask) >> i;

        G1 = A ^ B;
        G2 = A & B;
        G3 = Ri_1 & G1;
        Ri = G3 | G2;
        S = Ri_1 ^ G1;

        resultat = resultat | (S << i); 
        
        Ri_1 = Ri;
        mask = mask << 1;
    }    

    return resultat;
}

short adder(short x, short y){
    short increment = y; // incremente x de y
    
    short A = 0;
    short B = 0;
    short Ri_1 = 0;
    short Ri = 0;
    short S = 0;

    // Variables G(x) pour les états des portes logiques dans l'additionneur
    short G1 = 0;
    short G2 = 0;
    short G3 = 0;

    short mask = 1;

    short resultat = 0;

    for(int i = 0; i < 16; i++){
        A = (x & mask) >> i;
        B = (increment & mask) >> i;

        G1 = A ^ B;
        G2 = A & B;
        G3 = Ri_1 & G1;
        Ri = G3 | G2;
        S = Ri_1 ^ G1;

        resultat = resultat | (S << i); 
        
        Ri_1 = Ri;
        mask = mask << 1;
    }    

    return resultat;
}

short muxSup(short entre1, short entre2, short commande){
    if(commande == 0)
        return entre1;
    if(commande == 1)
        return entre2;
    printf("\n\nERROR : commande inconnu dans le multiplexeur\n\n");
    exit(EXIT_FAILURE);
}

short muxInf(short entre1, short entre2, short entre3, short entre4, short commande){
    if(commande == 0)
        return entre1;
    if(commande == 1)
        return entre2;
    if(commande == 2)
        return entre3;
    if(commande == 3)
        return entre4;
    printf("\n\nERROR : commande inconnu dans le multiplexeur\n\n");
    exit(EXIT_FAILURE);
}

short zero(short x){
    if(x == 0)
        return 1;
    return 0;
}

short sub(short a,short b){
    short increment = b; // soustrait a de b
    
    short X = 0;
    short Y = 0;
    short Si_1 = 0;
    short Si = 0;
    short D = 0;

    // Variables G(x) pour les états des portes logiques dans l'additionneur
    short G1 = 0;
    short G2 = 0;
    short G3 = 0;

    short mask = 1;

    short resultat = 0;

    for(int i = 0; i < 16; i++){
        X = (a & mask) >> i;
        Y = (increment & mask) >> i;

        
        G1 = X ^ Y;
        G2 = Y & (X^1);
        G3 = Si & (G1^1);
        Si_1 = G3 | G2;
        D = Si ^ G1;

        resultat = resultat | (D << i); 
        
        Si = Si_1;
        mask = mask << 1;
    }    

    return resultat;
}

short ALU(short a, short b, short commande){
    
    // Arithmetic Instructions
    
    if((commande == add) || (commande == add_immediate))
        return adder(a,b);
    if( commande == subract )
        return sub(a,b);
    if( commande == multiply ){
        short res = 0;
        for(int i = 0; i < b; i ++)
            res = res + adder(a,b);
        return res;
    }

    // Data Transfer

    if(commande == load_word) // lw $1,100($2) $1=Memory[$2+100]
        return adder(a,b); //memoire[adder(a,b)];
    if(commande == store_word) // sw $1,100($2) Memory[$2+100]=$1
        return adder(a,b);

    // Conditional Branch

    if(commande == branch_or_equal){ // beq $1,$2,100 if($1==$2) go toPC+4+100
        return registreFile[a] == registreFile[b];
    } 

    if(commande == branch_on_not_equal){ // bne $1,$2,100 if($1!=$2) go to PC+4+100
        return registreFile[a] != registreFile[b];
    }
    // Unconditional Jump

    if(commande == jump){ // j 1000 go to address 1000
        return b+2;
    }

    // System Calls

    if(commande == eXit){
        unitControlRegister.STOP = 1;
        return 1;
    }

    printf(RED"\n\nERROR : Opération ( %d ) non reconnu par le CPU\n\n"DEFAULT_COLOR,commande);
    exit(EXIT_FAILURE);
}

short instructionMemory(short pc){
    unsigned short mot;
    //printf("PC = %d\n",pc);
    mot = memoire[pc];
    //printf("mot = %d\n",mot);
    mot = mot << 8;
    //printf("mot = %d\n",mot);
    //printf("memoire + 1 = %d\n",memoire[pc+1]);
    mot = adder(mot,memoire[pc+1]);
    //printf("mot = %d\n",mot);

    return mot;
}