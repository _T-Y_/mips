#include <stdio.h>

#include "../headers/ordonnanceur.h"

void initOrdionnanceur(void){
    for(int i = 1;i < 10; i++){
        ordonnanceur[i] = -1;
    }
}

void saveOrdonnanceur(short process){
    //printf("process : %d\t",process % 10);
    ordonnanceur[process % 10] = 1;
}

int startOrdonnanceur(void){
    int start = -1;
    int bloc = 0;
    for(int i = 9; i >=0; i--){
        if( (ordonnanceur[i] != -1) && (bloc == 0)){
            start = i;
        }
        if((ordonnanceur[i] == -1) && (start != -1)){
            bloc = 1; // bloc la valeur de start
        }
    }
    return start;
}