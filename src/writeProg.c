#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "../headers/memoire.h"
#include "../headers/writeProg.h"
#include "../headers/color.h"

/*======================================================================================*/
/****************************************************************************************/
/*                                       FONCTION                                       */
/*------------------                                                  ------------------*/

/*
* Convertir la tableau binaire en entier
*/
unsigned char traduire(int *tab){

    unsigned char chiffre = 0;
    unsigned char calcul = 1;

    for(int i = 7; i >= 0; i--)
        if(tab[i] == 1){
            //chiffre += pow(2,39-i);
            calcul  = 1;
            for(int x = 1; x <= 7-i; x++){
                calcul *= 2;
            //    printf("\nChiffre : %d\n",chiffre);
            }
            chiffre += calcul;
        }

    return chiffre;
}

void affichage(unsigned char val, int *tab, int indice){
    printf("\t%d\t|\t",indice);
    for(int i=0;i<8;i++)
        printf("%d",tab[i]);
    printf("\t|\t%d",val);
    printf("\n");
}

/*
* Recupere dans le fichier binaire les données, ligne par ligne, et 
* enregistre le bits du InstructionWord dans un tableau d'entiers.
*/

void writeData(char *arg,int localisation){
    FILE *fic;
    unsigned char Word;
    char num[1];
    int tab[8];
    int i = 0;

    int placement;

    if(localisation == instruction){
        placement = 0;
    }else if(localisation == data){
        placement = 64;
    }else{
        printf(RED"\n\nwriteProg.c : Type de fichier inconnu\n\n"DEFAULT_COLOR);
    }

    fic = fopen(arg,"r");

    if(fic != NULL){
        for(int j = 0+placement; j <64+placement; j++){
            if((num[0]=fgetc(fic)) != EOF){
                // ------------------------------------------- Convertir de char vers int
                for(i=0;i<8;i++){
                    tab[i]=atoi(num);
                    num[0]=fgetc(fic);
                }
                // ___________________________________________
                // ------------------------------------------- Convertir tableau binaire en unsigned long
                Word = traduire(tab);
                // ___________________________________________
                // ------------------------------------------- Met en mémoire la ligne d'instruction du programme
                memoire[j] = Word;

                affichage(Word, tab, j);
                
                
            }else{
                break;
            }
        }
        fclose(fic);
    }else{
        perror("fopen");
        exit(EXIT_FAILURE);
    }
}