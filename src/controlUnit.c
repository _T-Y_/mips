// Entité contôle

#include <stdio.h>

#include "../headers/controlUnit.h"
#include "../headers/instruction.h"
#include "../headers/color.h"


void unitControlInit(void){
    unitControlRegister.IR = 0;
    unitControlRegister.PC = 0;
    unitControlRegister.aleasBranchement = 0;
    unitControlRegister.optocode = 0;
    unitControlRegister.RD = 0;
    unitControlRegister.RS = 0;
    unitControlRegister.RT = 0;
    unitControlRegister.PCsequentiel = 0;
    unitControlRegister.IMM = 0; //  sign etendu du registre contenant la valeur immédiate
    unitControlRegister.ALUSrcA = 0;
    unitControlRegister.ALUSrcB = 0;
    unitControlRegister.STOP = 0;
    unitControlRegister.Branchement = 0;
    unitControlRegister.memoryAdress = 0;
    unitControlRegister.dataRegister = 0;
    unitControlRegister.lecture1 = 0;
    unitControlRegister.lecture2 = 0;
    unitControlRegister.ecriture = 0;
    unitControlRegister.ordonnanceur = 0;
    unitControlRegister.RSadresse = 0;
    unitControlRegister.RTadresse = 0;
    unitControlRegister.aleasDonnees = 0;
}

short unitControlPcHorloge(short pc){
    unitControlRegister.PC = pc;
    return unitControlRegister.PC;
}

/*
    Revois l'optocode
*/
char unitControlOptocode(unsigned short IR){
    short mask = 61440; // 0b1111000000000000
    return (IR & mask) >> 12;
}

short unitControlIMM(unsigned short optocode, unsigned short IR){
    short mask;
    short sign;
    short val;

    if((optocode == add_immediate) || (optocode == branch_or_equal) || (optocode == branch_on_not_equal)){
        mask = 15; // 0b0000000000001111
        val = IR & mask;
        sign = val >> 4;
        sign = sign << 15;
        val = val | sign;
        return val;
    }

    if( (optocode == load_word) || (optocode == store_word) ){
        mask = 240; // 0b0000000011110000
        val = (IR & mask) >> 4;
        sign = val >> 4;
        sign = sign << 15;
        val = val | sign;
        return val;
    }

    return -1;
}

/*
    Les muxs permettent de faie un choix pour l'ALU
*/

void unitControlCommandMUX(short optocode){
    // depend de si cest un jump
    if(optocode == jump){ 
        unitControlRegister.ALUSrcA = 0; // PCSequentiel
        unitControlRegister.ALUSrcB = 0; // RD
        return;
    }
    if((optocode == add_immediate) ){
        unitControlRegister.ALUSrcA = 1; // registre RS
        unitControlRegister.ALUSrcB = 3; // registre IMM
        return;
    }
    if((optocode == branch_or_equal) || (optocode == branch_on_not_equal)){
        unitControlRegister.ALUSrcA = 1; // registre RS
        unitControlRegister.ALUSrcB = 0; // registre RD
        return;
    }
    if( (optocode == load_word) || (optocode == store_word) ){
        unitControlRegister.ALUSrcA = 1; // registre RS
        unitControlRegister.ALUSrcB = 3; // registre IMM
        return;
    }
    
    unitControlRegister.ALUSrcA = 1; // registre RS
    unitControlRegister.ALUSrcB = 1; // registre RT
}


/*
    Permet d'avoir l'affichage del'optocode
*/
void unitControlPrintOptocode(short op){
    if(op == add)
        printf("add\n");
    if(op == subract)
        printf("substract\n");
    if(op == multiply)
        printf("multiply\n");
    if(op == add_immediate)
        printf("add immediate\n");
    if(op == load_word)
        printf("lord word\n");
    if(op == store_word)
        printf("store word\n");
    if(op == branch_or_equal)
        printf("branch or equal\n");
    if(op == branch_on_not_equal)
        printf("branch on not equal");
    if(op == jump)
        printf("jump\n");
    if(op == eXit)
        printf("exit\n");
}

/*
    Affiche l'avancé des cycles des inbstructions
*/
void unitcontrolePrintCycle(short tab[], int etage){
    for(int i = 0;i < 10; i++){
        if(tab[i] == 0){
            if(etage != i)
                printf(""RED"IF"DEFAULT_COLOR" - ");
            else
                printf("[ "RED"IF"DEFAULT_COLOR" ] - ");
        }
        if(tab[i] == 1){
            if(etage != i)
                printf(""RED"ID"DEFAULT_COLOR" - ");
            else
                printf("[ "RED"ID"DEFAULT_COLOR" ] - ");
        }
        if(tab[i] == 2){
            if(etage != i)
                printf(""RED"EX"DEFAULT_COLOR" - ");
            else
                printf("[ "RED"EX"DEFAULT_COLOR" ] - ");
        }
        if(tab[i] == 3){
            if(etage != i)
                printf(""RED"MEM"DEFAULT_COLOR" - ");
            else
                printf("[ "RED"MEM"DEFAULT_COLOR" ] - ");
        }
        if(tab[i] == 4){
            if(etage != i)
                printf(""RED"WB"DEFAULT_COLOR" - ");
            else
                printf("[ "RED"WB"DEFAULT_COLOR" ] - ");
        }
        if(tab[i] == 5){
            if(etage != i)
                printf(""RED"BULLE"DEFAULT_COLOR" - ");
            else
                printf("[ "RED"BULLE"DEFAULT_COLOR" ] - ");
        }

        if(tab[i] == -1)
            break;
    }
}