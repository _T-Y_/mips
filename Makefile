CC = gcc
CFLAGS = -Wall -pthread
HEADERS = headers/CPU.h headers/registerFile.h headers/writeProg.h headers/memoire.h headers/controlUnit.h headers/ordonnanceur.h #headers/registres.h headers/memoire.h
SRC = $(wildcard src/*.c)
OBJ = bin/main.o bin/CPU.o bin/registerFile.o bin/writeProg.o bin/memoire.o bin/controlUnit.o bin/ordonnanceur.o
EXEC = MIPS

include .color

bin/%.o: src/%.c $(HEADERS)
	@$(CC) -o $@ -c $< $(CFLAGS)
	@echo "[$(OK_COLOR) OK $(NO_COLOR)] Create files object : $(OK_COLOR)$@$(NO_COLOR) "

$(EXEC) : $(OBJ)
	@$(CC) -o $@ $(OBJ) $(CFLAGS)
	@echo "[$(OK_COLOR) OK $(NO_COLOR)] Create executable : $(OK_COLOR)$(EXEC)$(NO_COLOR)"

help:
	@echo "\n"
	@echo "=================================================================\n"
	@echo "$(COM_COLOR)\t\t\t     HELP\n$(NO_COLOR)"
	@echo "=================================================================\n"
	@echo "$(COM_COLOR)make\n$(NO_COLOR)"
	@echo "$(COM_COLOR)clear              clean all object files$(NO_COLOR)"
	@echo "$(COM_COLOR)clearAll           clean all object files and executables$(NO_COLOR)"
	@echo "$(COM_COLOR)$(EXEC)               run application$(NO_COLOR)\n"
	@echo "=================================================================\n"
	@echo "$(COM_COLOR)Pour executer le programme : ./$(EXEC) 'instruction'.txt 'data'.txt$(NO_COLOR)\n"
	@echo "=================================================================\n"
	@echo "$(COM_COLOR)Exemple présent : $(NO_COLOR)./MIPS instruction/2boucle.txt data/1data.txt\n"
	@echo "=================================================================\n"

clear:
	@echo "\n"
	@echo "[$(WARN_COLOR) DELETE $(NO_COLOR)] Object files : $(WARN_COLOR)$(OBJ)$(NO_COLOR)"
	@rm -rf $(OBJ)

clearAll: clear
	@echo "[$(WARN_COLOR) DELETE $(NO_COLOR)] Executables : $(WARN_COLOR)$(EXEC)$(NO_COLOR)"
	@rm -rf $(EXEC)
	@echo "\n"

.PHONY: $(EXEC) clear clearAll