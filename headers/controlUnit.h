#ifndef HEADER_CONTROLE_UNIT_H
#define HEADER_CONTROLE_UNIT_H

    struct controlU{
        unsigned short IR;
        char PC;
        char PCsequentiel;
        char RD;
        char RS;
        char RT;
        short IMM;
        char optocode;
        short ALUSrcA; // commandeMUXsup
        short ALUSrcB; // commandeMUXinf

        short STOP;//Stop program from running
        
        int Branchement;
        int aleasBranchement;
        int aleasDonnees;

        short memoryAdress;
        short dataRegister;

        short lecture1;
        short lecture2;
        short ecriture;

        short ordonnanceur;

        short RTadresse;
        short RSadresse;
    };

    typedef struct controlU controlU;
    controlU unitControlRegister;

    void unitControlInit(void);
    short unitControlPcHorloge(short pc);
    char unitControlOptocode(unsigned short IR);
    short unitControlIMM(unsigned short optocode, unsigned short IR);
    void unitControlCommandMUX(short optocode);
    void unitControlPrintOptocode(short op);
    void unitcontrolePrintCycle(short tab[], int etage);

#endif