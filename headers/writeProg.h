#ifndef HEADER_WRITEPROG_H
#define HEADER_WRITEPROG_H

    enum memory{
        data,
        instruction
    };

    unsigned char traduire(int *tab);
    void affichage(unsigned char val, int *tab, int indice);
    void writeData(char *arg, int localisation);

#endif