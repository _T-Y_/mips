#ifndef HEADER_ORDONNANCEUR_H
#define HEADER_ORDONNANCEUR_H

    short ordonnanceur[10];

    void initOrdionnanceur(void);
    void saveOrdonnanceur(short process);
    int startOrdonnanceur(void);
    void seqOrdonnanceur(void);

#endif