#ifndef HEADER_CPU_H
#define HEADER_CPU_H

    short adder2(short pc);
    short adder(short x, short y);
    short muxSup(short entre1, short entre2, short commande);
    short muxInf(short entre1, short entre2, short entre3,short entre4, short commande);
    short zero(short x);
    short sub(short a,short b);
    short ALU(short a, short b, short commande);
    short instructionMemory(short pc);

#endif