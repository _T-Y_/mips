#ifndef HEADER_INSTRUCTION_H
#define HEADER_INSTRUCTION_H

    // Arithmetic Instructions
    #define add                     1
    #define subract                 2
    #define add_immediate           3
    #define multiply                4
    
    // Data Transfer
    #define load_word               5
    #define store_word              6
    
    // Conditional Branch
    #define branch_or_equal         7
    #define branch_on_not_equal     8
    
    // Unconditional Jump
    #define jump                    9
    
    // System Calls
    #define eXit                    10

#endif