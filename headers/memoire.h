#ifndef HEADER_MEMOIRE_H
#define HEADER_MEMOIRE_H


    /*
    – La capacité = 2k Mots mémoire
    – La capacité = 2k * n Bits

    Donc 2^7 = 128 mots 
    Et 2^7 * 8 = 1024 = 1ko
    */

    // char = 1 octects = 8 bits

    // un mot du registre file est composé de 2 mot dans la mémoire
    // la moitier de la mémoire est pour les instructions, l'autre pour les données.
    // Donc il y a 128 / 2 "mots mémoires" / 2 "capacité de stockage d'instructions" instructions au total.
    // Cela fait 32 instructions possible

    #define MAX 128

    unsigned char memoire[MAX];
    //char maskMem = 127; // 0b01111111 Cela permet de ne pas depasser la capacité total de la mémoire

    void initMemoire();

#endif